import Vue from "vue";
import "vue-awesome/icons";
// import Icon from "vue-awesome/components/Icon.vue";
// @ts-ignore
import AppVue from "./App.vue";
import router from "./router";
// import store from "./store";
// import { i18n } from "./locales/i18n";
import Buefy from "buefy";

Vue.use(Buefy);

// Vue.component("v-icon", Icon);
Vue.config.productionTip = false;
new Vue({
  router,
  render: (h: any) => h(AppVue),
}).$mount("#app");
